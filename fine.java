package com.example.vl13;

import java.time.LocalDate;

public class fine
{
    public int id;
    public String car_number;
    public String violator_full_name;
    public String policeman_full_name;
    public String protocol_compiler_full_name;
    public Double fine_amount;
    public Boolean is_fine_sent_to_court;
    public Boolean is_fine_paid;
    public LocalDate protocol_date;
    public LocalDate fine_payment_date;
    public LocalDate fine_payment_date_deadline;
}
