package com.example.vl13;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("api/v1/fines")
public class controller
{
    fines_database database;

    controller()
    {
        database = new fines_database();
    }

    @GetMapping
    public ArrayList<fine> get_all()
    {
        return database.data;
    }

    @PostMapping
    public String add(@RequestBody fine value)
    {
        database.add(value);
        return "Fine id " + value.id + " added";
    }

    @PutMapping
    public String edit(@RequestBody fine value)
    {
        database.set(value);
        return "Fine id " + value.id + " edited if exists";
    }

    @DeleteMapping
    public String del(@RequestBody fine value)
    {
        database.remove(value.id);
        return "Fine id " + value.id + " deleted if exists";
    }

    @GetMapping(value = "/{id}")
    public fine get_by_index(@PathVariable("id") int id)
    {
        return database.get_by_id(id);
    }

    //@Transactional - почему не определяется?
    @PatchMapping(value = "/{id}/pay")
    public String pay(@PathVariable("id") int id)
    {
        fine tmp = database.get_by_id(id);
        if (tmp != null)
        {
            //... Логика оплаты
            tmp.is_fine_paid = true;
            database.set(tmp);
            return "Fine id " + tmp.id + " is paid";
        }
        else
        {
            return "Fine id " + id + " is not found";
        }
    }

    @PatchMapping(value = "/{id}/court")
    public String court(@PathVariable("id") int id)
    {
        fine tmp = database.get_by_id(id);
        if (tmp != null)
        {
            //... Логика отправления в суд
            tmp.is_fine_sent_to_court = true;
            database.set(tmp);
            return "Fine id " + tmp.id + " is sent to court";
        }
        else
        {
            return "Fine id " + id + " is not found";
        }
    }
}