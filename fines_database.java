package com.example.vl13;

import java.util.ArrayList;

public class fines_database
{
    ArrayList<fine> data;

    fines_database()
    {
        data = new ArrayList<>();
    }

    public boolean is_empty()
    {
        return data.size() == 0;
    }

    public boolean contains(fine value)
    {
        for (fine fine : data)
        {
            if (fine == value)
            {
                return true;
            }
        }

        return false;
    }

    public boolean contains(int id)
    {
        for (int i = 0; i < data.size(); i++)
        {
            if (data.get(i).id == id)
            {
                return true;
            }
        }

        return false;
    }

    public void add(fine fine)
    {
        fine.id = data.size() + 1;
        data.add(fine);
    }

    public void set(fine fine)
    {
        for (int i = 0; i < data.size(); i++)
        {
            if (data.get(i).id == fine.id)
            {
                data.set(i, fine);
                break;
            }
        }
    }

    public void remove(fine value)
    {
        for (int i = 0; i < data.size(); i++)
        {
            if (data.get(i) == value)
            {
                data.remove(i);
                break;
            }
        }
    }

    public void remove(int id)
    {
        for (int i = 0; i < data.size(); i++)
        {
            if (data.get(i).id == id)
            {
                data.remove(i);
                break;
            }
        }
    }

    public void clear()
    {
        data.clear();
    }

    public int size()
    {
        return data.size();
    }

    public fine get_by_id(int id)
    {
        for (int i = 0; i < data.size(); i++)
        {
            if (data.get(i).id == id)
            {
                return data.get(i);
            }
        }

        return null;
    }

    public fine get_by_index(int index)
    {
        return data.get(index);
    }

    public ArrayList<fine> get_all()
    {
        return data;
    }
}
