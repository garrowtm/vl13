package com.example.vl13;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class vl_13_app
{

    public static void main(String[] args)
    {
        SpringApplication.run(vl_13_app.class, args);
    }

}
